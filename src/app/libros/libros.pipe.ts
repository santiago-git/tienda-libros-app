import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroLibros'
})
export class LibrosPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if(!value) return [];
    if(!args) return value;

    args = args.toLowerCase();
    return value.filter( it => { return it.nombre.toLowerCase().includes(args) });
   }

}