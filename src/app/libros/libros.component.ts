import { Component, OnInit, Renderer2 } from '@angular/core';
import { LibrosService } from "./libros.service"
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LibrosPipe } from './libros.pipe';
import { routerTransition } from '../router.animations';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css'],
  animations: [routerTransition()]
})
export class LibrosComponent implements OnInit {

  private filtro: String;

  antiguo: HTMLElement;

  constructor(private http: Http,
    private renderer: Renderer2,
    private LibrosService: LibrosService) { }

  ngOnInit() { }

  mostrarActivo(elemento: HTMLElement, libro) {
    libro.seleccionado = !libro.seleccionado;
    elemento.setAttribute("data-seleccionado", "true");
  }

}