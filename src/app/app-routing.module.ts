import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { LibrosComponent } from './libros/libros.component';
import { DetallesComponent } from './detalles/detalles.component';


const routes: Routes = [
  // {
  //   path: '',
  //   children: []
  // },
  { path: 'login', component: LoginComponent, data: { title: "Login" }},
  { path: 'libros', component: LibrosComponent },
  { path: 'detalles/:libroId', component: DetallesComponent },
  { path: '**', redirectTo: 'login' },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
