import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

import { LibrosService } from './libros/libros.service';

import { MaterializeModule } from 'angular2-materialize';
import { LoginComponent } from './login/login.component';
import { LibrosComponent } from './libros/libros.component';
import { DetallesComponent } from './detalles/detalles.component';
import { LibrosSeleccionadosComponent } from './libros-seleccionados/libros-seleccionados.component';
import { LibrosSeleccionadosService } from './libros-seleccionados/libros-seleccionados.service';
import { LibrosPipe } from './libros/libros.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LibrosComponent,
    DetallesComponent,
    LibrosSeleccionadosComponent,
    LibrosPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterializeModule,
    FormsModule,
    HttpModule,
  ],
  providers: [
    LibrosService,
    LibrosSeleccionadosService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
