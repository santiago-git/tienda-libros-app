import { Component, OnInit } from '@angular/core';
import { LibrosService } from '../libros/libros.service';

@Component({
  selector: 'app-libros-seleccionados',
  templateUrl: './libros-seleccionados.component.html',
  styleUrls: ['./libros-seleccionados.component.css']
})
export class LibrosSeleccionadosComponent implements OnInit {

  constructor(private librosSeleccionados: LibrosService) { }

  ngOnInit() {}

  removerLibro(libro) {
    libro.seleccionado = !libro.seleccionado;
  }
}